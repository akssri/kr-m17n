;;; brahmi.el -- lipi interchange for brahmi

;; Copyright (C) 2018 Akshay Srinivasan <akshaysrinivasan@gmail.com>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2 of
;; the License, or (at your option) any later version.
;;          
;; This program is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the implied
;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;; PURPOSE.  See the GNU General Public License for more details.
;;          
;; You should have received a copy of the GNU General Public
;; License along with this program; if not, write to the Free
;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
;; MA 02111-1307 USA

;; Usage:
;; - Place the script in a suitable place that Emacs knows about (eg. ~/.emacs.d/lisp)
;; - Include '(require 'brahmi)' in ~/.emacs

(defvar brahmi-unicode-blocks
  '((nagari #x0900)
    (kannada #x0c80)
    (grantha #x11300)
    (malayalam #x0d00)
    (tamil #x0b80)
    (telugu #x0c00);;
    (bengali #x0980)
    (gujrati #x0a80)
    (oriya #x0b00)
    (gurmukhi #x0a00)
    (sinhala #x0d80)
    ;;(sharada #x11180) ;weird blocks
    ;;(siddham #x11580)
    (brahmi #x11000)))

(defun brahmi-convert (from to string)
  (let* ((fstart (second (or (assoc from brahmi-unicode-blocks)
			     (error "Can't find block `%s'" from))))
	 (tstart (second (or (assoc to brahmi-unicode-blocks)
			     (error "Can't find block `%s'" to)))))
    (map 'string (lambda (x) (if (<= fstart x (+ fstart 128)) (+ (- x fstart) tstart) x)) string)))

(defun replace-brahmi (begin end from to)
  (interactive (list (region-beginning) (region-end)
		     (completing-read "Replace script in region: " (mapcar (lambda (x) (car x)) brahmi-unicode-blocks))
		     (completing-read "Replace with: " (mapcar (lambda (x) (car x)) brahmi-unicode-blocks)))) 
  (let ((text (buffer-substring-no-properties begin end)))
    (delete-region begin end)
    (insert (brahmi-convert (intern from) (intern to) text))))

(provide 'brahmi)
